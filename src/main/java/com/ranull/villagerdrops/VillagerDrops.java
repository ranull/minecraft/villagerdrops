package com.ranull.villagerdrops;

import com.ranull.villagerdrops.events.Events;
import org.bukkit.plugin.java.JavaPlugin;

public class VillagerDrops extends JavaPlugin {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new Events(), this);
    }
}
