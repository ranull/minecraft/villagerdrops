package com.ranull.villagerdrops.events;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class Events implements Listener {
    @EventHandler
    public void onVillagerDeath(EntityDeathEvent event) {
        if (event.getEntityType().equals(EntityType.VILLAGER)) {
            Villager villager = (Villager) event.getEntity();

            for (ItemStack itemStack : villager.getInventory()) {
                if (itemStack != null) {
                    event.getDrops().add(itemStack);

                    villager.getInventory().removeItem(itemStack);
                }
            }

            event.getDrops().add(new ItemStack(Material.EMERALD, new Random().nextInt((3 - 1) + 1) + 1));
        }
    }
}
